TDIR=`mktemp -d`

trap "{ cd - ; rm -rf $TDIR; exit 255; }" SIGINT

cd $TDIR
	apt-get install mongodb-dev
	wget https://raw.github.com/pypa/pip/master/contrib/get-pip.py
	python get-pip.py
	pip install -r /vagrant/requirements.txt
cd -

rm -rf $TDIR

exit 0