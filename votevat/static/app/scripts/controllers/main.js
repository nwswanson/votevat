'use strict';

angular.module('votevatApp')
	.controller('MainCtrl', function ($scope, Polladmin, User) {
		$scope.pollname = "My Great Poll";
		$scope.login = false;
		var refresh = function() {
			Polladmin.getAllMyPolls().then(function(d){
				$scope.polls = d.data;
			});
		};

		$scope.$watch(User.userDetails, function(n){
				$scope.login = n.active;
		}, true);

		refresh();


		$scope.newpoll = function(name) {
			Polladmin.createNewPoll(name).then(function(d){
				refresh();
			});
		};
		$scope.delpoll = function(pollid) {
			Polladmin.deletePoll(pollid).then(function(d){
				refresh();
				console.log('hey')
			})
		}
	});
