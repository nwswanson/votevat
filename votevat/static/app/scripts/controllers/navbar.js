'use strict';

angular.module('votevatApp')
.controller('NavbarCtrl', function ($scope, User) {
		$scope.active = false;
		$scope.name = '';
		$scope.flash_error = false;
		$scope.register_box = false;

		$scope.$watch(User.userDetails, function(n, o){
//			var x = User.userDetails();
			$scope.name = n.name;
			$scope.active = n.active;
		}, true);

		$scope.login = function(user, pass) {
				User.logIn(user, pass).then(function(d){
//					do something.
					if (d.data && d.data.status === "good") {
						$scope.active = true;
						console.log($scope.active);
					}
				})
		};
		$scope.pop_register = function(){
//				dipslay the register dialog.
		};
		$scope.register = function(user, pass){
			if (user && pass) {
				User.register(user, pass);
			} else {
				$scope.flash_error = true;
			}

		};
		$scope.logout = function() {
//			log out the user.
//			returns a promise, but currently with the watch function nothing
//			else needs to be cleaned up, so the return is ignored.
			User.logOut();
		};

});
