'use strict';

angular.module('votevatApp')
  .controller('EditCtrl', function ($scope, Polladmin, $routeParams) {
		$scope.optionname = "My New Poll Option";


		Polladmin.getCurrentPoll($routeParams.pollId).then(function(d){
			$scope.poll = d.data;
			$scope.locked = d.data.locked;
		});

		$scope.addOption = function(opt){

			Polladmin.addItem($routeParams.pollId, opt).then(function(d){

				$scope.poll.options.push(d.data[0]);
			})
		};
		$scope.publish = function() {
			Polladmin.pushLive($routeParams.pollId)
				.then(function(d){
					$scope.locked = true;
				});
		};
		$scope.delitem = function(voteid){
			Polladmin.deleteItem($routeParams.pollId, voteid).then(function(d){
				$scope.poll.options = d.data;
			})
		}

  });
