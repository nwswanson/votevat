'use strict';

angular.module('votevatApp')
	.controller('VoteCtrl', function ($scope, Ballots, $routeParams) {

		Ballots.getBallot($routeParams.pollId).then(function(d){
			$scope.unvoted = d.unvoted;
			$scope.ranked = d.ranked;

		});


		$scope.clearAll = function() {
			$scope.unvoted = $scope.unvoted.concat($scope.ranked);
			$scope.ranked = [];
		};
		$scope.vote = function(){
			Ballots.submitBallot($routeParams.pollId);
		};
		$scope.sortableOptions = {
			placeholder: "vote-insert",
			connectWith: ".vote-container",
			update: function(e, ui) {
			//this is where the service will be updated.
				Ballots.syncLists($scope.unvoted, $scope.ranked);

			}
		};


	});
