'use strict';

angular.module('votevatApp')
  .service('User', function User($http, $route) {
    // AngularJS will instantiate a singleton by calling "new" on this function
		var active;
		var name;
        return {
			register: function(user, password) {
				return $http.get('/register'+user+"/"+password);
			},
			logIn: function(user, password){
				return $http.get('/api/auth/login/'+user+'/'+password).success(function(d){
						console.log(d);
						active = d.status === "good";
						name = d.name || 'Guest';
						$route.reload();
						return d;
				});
			},
			logOut: function(){
				return $http.get('/api/auth/logout').success(function(d){
					active = false;
					$route.reload();
					name = 'Guest';
				})
			},
            getLogin: function() {
                return $http.get('/api/auth/who')
					.success(function(d){
						active = d.session;
						name = d.name;
						return d;
					})
					.then(function(d){
						//mangle the data here
						return d;
					})
            },
			active: function (){
				return active;
			},
            userDetails: function() {
                return {
                    'name': name,
					'active': active

                }
            }
        }
  });
