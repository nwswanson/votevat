'use strict';
//change this into a constructor function?
var tfunc = function(object){
	return object.data;
};

angular.module('votevatApp')
  .service('Polladmin', function Polladmin(User, $http, $location) {
    // AngularJS will instantiate a singleton by calling "new" on this function
		var clean_data = function(data) {
			return data;
		};

//		User.getLogin().then(function(d){
//
//			console.log(d)
//		});
		return {
			getAllPolls: function() {
				return $http.get('/api/pleb/poll/open').then(tfunc);
			},
			createNewPoll: function(name) {
				return $http.get('/api/pleb/poll/create/'+name).then(tfunc);
			},
			deletePoll: function(pollid) {
				return $http.get('/api/pleb/delete/'+pollid).then(tfunc);
			},
			updatePoll: function(pollid, data) {
				return $http.post('/api/pleb/modify/'+pollid, clean_data(clean_data));
			},
			addItem: function(pollid, title, notes) {
				return $http.post('/api/pleb/poll/'+pollid+'/add_opt', {
						'title': title,
						'notes': notes
				}).then(tfunc);
			},
			pushLive: function(pollid) {
				return $http.post('/api/pleb/poll/'+pollid+'/publish', {publish:true})
					.success(function(d){
						$location.path('/list');
					}).then(tfunc);
			},
			deleteItem: function(pollid, itemid) {
				return $http.post('/api/pleb/poll/'+pollid+'/del_opt/'+itemid)
							.then(tfunc);
			},
			getAllMyPolls: function() {
				return $http.get('/api/pleb/poll/mine').then(tfunc);
			},
			getCurrentPoll: function(pollid) {
				return $http.get('/api/pleb/poll/'+pollid).then(tfunc)
			}
		}
  });
