'use strict';

angular.module('votevatApp')
  .service('Ballots', function Ballots($http, $q) {
	// AngularJS will instantiate a singleton by calling "new" on this function
        var currentBallot = "";
        var unvoted = [];
        var ranked = [];
		var currentid = '';
		var canVote = true;

        return {
			getBallot: function(ballotId){
//				Cache a copy of the list locally, so that we can re-build the
//				controller if the user switches views, but doesn't change to
//				a different ballot
				if (currentid === ballotId){
					var prom = $q.defer();
					prom.resolve({
						unvoted: unvoted,
						ranked: ranked,
						canVote: canVote
					});
					return prom.promise;
				}
				return $http.get('/api/pleb/poll/'+ballotId).success(function(d){
					if (d.data) {
						currentid = ballotId;
						console.log(d)
						unvoted = d.data.options;
						ranked = [];
					} else {
//						TODO: Handle error here.
						currentid = ballotId;
						unvoted = [];
						unvoted = [];
						return d;
					}

					return d
				}).then(function(d){
						return {
							unvoted: unvoted,
							ranked: ranked,
							canVote: canVote
						}
				})
			},
			submitBallot: function(ballotId){
//				console.log('hey');
				return $http.post('/api/pleb/poll/'+ballotId+'/vote', ranked);
			},
			canVote: function(){
				return canVote;
			},
            syncLists: function(_unsorted, _ranked) {
                unvoted = _unsorted;
                ranked = _ranked;
            }


        }
  });
