from . import db
import datetime
from uuid import uuid4
from mongoengine import PULL
from bson import ObjectId
def str_uuid():
    return str(uuid4()).replace('-','')

class User(db.Document):
    """
        A user. Name, Password, Ballots they've voted on.
    """
    name = db.StringField(max_length=255, required=True, unique=True)
    #TODO: change this to a pw hash
    insecure_password = db.StringField(max_length=255, required=True)
    #use a function/lambda so it doesn't execute immediately
    token = db.StringField(max_length=255, default=str_uuid)
    admin = db.BooleanField(default=False)
    ballots_on = db.ListField(db.ReferenceField('BallotBox'))

    def my_votes(self):
        return Ballot.objects.filter(created_by=self)

    def my_polls(self):
        return BallotBox.objects.filter(created_by=self)


class VoteOption(db.Document):
    title = db.StringField(max_length=255, required=True)
    notes = db.StringField(max_length=255)


class Ballot(db.Document):
    """
        The document that someone could vote on.
    """
    created_by = db.ReferenceField(User, required=True)
    open = db.BooleanField(default=True)
    unranked = db.ListField(db.ReferenceField('VoteOption',
                                             reverse_delete_rule=PULL))
    ranked = db.ListField(db.ReferenceField('VoteOption',
                                            reverse_delete_rule=PULL))

    for_poll = db.ReferenceField('BallotBox')

    def finish_ballot(self, item_list):
        #objlist = [ObjectId(id) for id in item_list]
        allitems = self.ranked + self.unranked

        matching = filter(lambda o: str(o['id']) in item_list, allitems)
        votes = sorted(matching, key=lambda v:item_list.index(str(v['id'])))
        unranked = [vote for vote in allitems if vote not in matching]

        self.unranked = unranked
        self.ranked= votes

        self.open = False
        self.save()
        return self


class BallotBox(db.Document):
    """

        The Collection of votes for tallying up.
    """
    created_at = db.DateTimeField(default=datetime.datetime.now)
    created_by = db.ReferenceField(User, required=True)
    title = db.StringField(max_length=255, required=True)
    url_guid = db.StringField(unique=True, max_length=255,
                              default=lambda: str_uuid()[:10])
    open = db.BooleanField(default=True)
    # the difference between open and visible is that open is voteable,
    # but visible is able to be seen by other users.
    # to vote on something the ballot box must be open and visible.
    visible = db.BooleanField(default=False)
    # can just filter out anything that isn't finalized by poll close.
    ballots = db.ListField(db.ReferenceField('Ballot',
                                             reverse_delete_rule=PULL))
    options = db.ListField(db.ReferenceField('VoteOption',
                                             reverse_delete_rule=PULL))

    def new_ballot(self, user, title):
        pass

    @property
    def editable(self):
        return self.open and not self.visible

    @property
    def votable(self):
        """
        :return: bool, if a user can vote on something.
        """
        return self.open and self.visible

    def can_vote(self, user=None):
        print user
        if not user:
            return False
        test = Ballot.objects(created_by=user, for_poll=self)
        # if the user doesn't have anything open, user can create a poll.
        if not test:
            return True
        if test[0].open is True:
            return True
        return False

    def get_user_ballot(self, user):
        test = Ballot.objects(created_by=user, for_poll=self)
        if test:
            return test[0]
        else:
            return self._create_ballot_for_user(user)

    def _create_ballot_for_user(self, user):
        ballot = Ballot(created_by=user, for_poll=self)
        ballot.unranked.extend(self.options)
        ballot.save()
        self.ballots.append(ballot)
        self.save()
        return ballot

    def add_options(self, list_of_options):
        opts = []
        for item in list_of_options:

            v = VoteOption(title=item.get('title') or "Vote Item")
            v.save()
            opts.append(v)
        self.options.extend(opts)
        self.save()
        return opts


    def delete_option(self, option_id):
        option = VoteOption.objects(id=option_id)
        if option and option[0] in self.options:
            option.delete()
            self.save()
        return

    def condorcet(self):
        pass

    def borda(self):

        """
            borda vote

        """
        # To avoid 'bullet' voting, the remainder votes should be distributed
        # amongst the unranked votes. the remander is sum((Nt - Nv))
        dist_votes = lambda N_to, total: sum(xrange(1, ((total - N_to)+1)))/total
        NumAllocated = 1

        pass

    def __unicode__(self):
        return self.title

