from flask import Blueprint, request, current_app, Response
from mongoengine import NotUniqueError
from ..models import BallotBox, VoteOption
from ..helpers import is_logged_in, get_user, user_can_change_poll, get_poll
from functools import wraps
from flask import g, request, redirect, url_for
from json import dumps, loads
from uuid import uuid4
from bson import json_util

votesystem = Blueprint('voting_system', __name__, url_prefix='/api/pleb')


# not_authorized = '{"issue":"not_logged_in"}'


def response_builder(data, good=True, message=None, json=True, indent=4):
    """
    Response builder. Convenience method for building a standard resopnse for
    angular to parse, so that everything is in the same location.

    :param good: bool for whether the action went ok
    :param message: informative message to flash
    :param data: data for the action
    :param json: should the result be turned into a json string
    :return: str or dict, based on json
    """
    ret = dict(
        action="ok" if good else "issue",
        message=message,
        data=data
    )
    if not json:
        return ret
    return dumps(ret, default=json_util.default, indent=indent)


def login_required(restrict_user=False, message=None):
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            user = get_user()
            if not user:
                return dumps(dict(issue=message or "Login Required"))
            return f(*args, **kwargs)
        return decorated_function
    return decorator

#def login_required(f):
#    @wraps(f)
#    def decorated_function(*args, **kwargs):
#        print get_user().name
#        return f(*args, **kwargs)
#    return decorated_function



@votesystem.route('/poll/create/<title_string>')
def create_new_poll(title_string):
    if not is_logged_in():
        return "bad"
    user = get_user()
    new_ballot = BallotBox(created_by=user, title=title_string)
    new_ballot.save()
    return response_builder(new_ballot.url_guid)




def create_item(x, user=None):
    d = dict(url=x.url_guid,
             _id=str(x.id),
             title=x.title,
             open=x.votable,
             votes=len(x.ballots),
             can_vote=x.can_vote(user),
             edit=x.editable,
             date=x.created_at.isoformat())

    return d

@votesystem.route('/poll/mine')
@login_required()
def list_my_polls():
    user = get_user()
    if not user:
        return "{}"
    my_stuff = [create_item(pol, user) for pol in user.my_polls()]
    return response_builder(my_stuff)

@votesystem.route('/poll/open')
def list_all_polls():
    print get_user()
    items = (i for i in BallotBox.objects if i.votable)
    return response_builder([create_item(pol, get_user()) for pol in items])


@votesystem.route('/poll/<pollname>')
def view_poll_info(pollname):
    ballot = BallotBox.objects(url_guid=pollname)
    #print([i.title for i in ballot[0].options])
    if ballot:
        b = ballot[0]
        r = dict(title=b.title, url=b.url_guid, locked=b.votable)
        r['options'] = [dict(title=i.title,
                             id=str(i.id)) for i in ballot[0].options]
        return response_builder(r)
    else:
        return response_builder(None, good=False, message="No Poll Found")




@votesystem.route('/delete/<pollid>')
def delete_poll(pollid):
    user = get_user()
    if not user:
        return response_builder(None, good=False, message="Not Authorized.")
    #TODO: put in superuser logic as well.
    if user_can_change_poll(user, pollid):
        get_poll(pollid).delete()
        return response_builder(None, message="Poll Deleted.")
    else:
        return response_builder(None, good=False,
                                message="You can't delete this.")


@votesystem.route('/poll/<pollid>/publish', methods=['POST'])
def publish_poll(pollid):
    user = get_user()
    if not user_can_change_poll(user, pollid):
        return response_builder(None, good=False, message="No Access")

    poll = get_poll(pollid)

    if poll:
        poll.visible = True
        poll.save()
    #print loads(request.data)

    return response_builder(None, message="Poll is live.")

@votesystem.route('/poll/<pollid>/add_opt', methods=['POST'])
def update_poll(pollid):
    user = get_user()
    if not user_can_change_poll(user, pollid):
        return response_builder(None, good=False, message="No Access")

    poll = get_poll(pollid)

    item = loads(request.data)
    results = [opt for opt in poll.add_options([item])]
    return response_builder([dict(title=i.title, id=str(i.id)) for i in results])


@votesystem.route('/poll/<pollid>/del_opt/<optionid>', methods=['POST'])
def delete_option(pollid, optionid):
    user = get_user()
    if not user_can_change_poll(user, pollid):
        return response_builder(None, good=False, message="You can't change this.")

    d = VoteOption.objects(id=optionid)
    if d:
        d[0].delete()
    poll = get_poll(pollid)

    opts = [dict(title=i.title, id=str(i.id)) for i in poll.options]

    return response_builder(opts)


@votesystem.route('/poll/<pollid>/vote', methods=['POST'])
@login_required()
def vote_for_poll(pollid):
    ballot = BallotBox.objects(url_guid=pollid)
    if ballot:
        ballot = ballot[0].get_user_ballot(get_user())
        ballot.finish_ballot([x['id'] for x in loads(request.data)])
    return response_builder(None, message="Your vote counts!")

@votesystem.route('/poll/<pollid>/close', methods=['POST'])
@login_required()
def close_and_tally(pollid):
    user = get_user()
    if not user_can_change_poll(user, pollid):
        return response_builder(None, good=False,
                                message="You can't change this.")
    ballot = BallotBox.objects(url_guid=pollid)
    if ballot:
        pass

    return response_builder(None, message="Vote was closed.")
