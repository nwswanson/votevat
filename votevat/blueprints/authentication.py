from flask import Blueprint, request, current_app, Response
from mongoengine import NotUniqueError
from ..models import User
from ..helpers import is_logged_in
from .votesystem import response_builder
from json import dumps
from uuid import uuid4

login = Blueprint('auth_module', __name__, url_prefix='/api/auth')

@login.app_errorhandler(405)
@login.app_errorhandler(500)
def badmethod(e):
    return response_builder(None, good=False, message="Server Error.")

@login.app_errorhandler(404)
def methodnotfound(e):
    return response_builder(None, good=False, message="Method not found.")

@login.route('/register/<uname>/<password>', methods=['GET'])
def create_user(uname, password):
    newuser = User(name=uname, insecure_password=password)
    try:
        newuser.save()
    except NotUniqueError, e:
        return dumps(dict(status="issue", desc="Name already exists."))
    resp = Response(dumps(dict(status="good", token=newuser.token)))
    resp.set_cookie('token', value=newuser.token)
    return resp

@login.route('/login/<uname>/<password>', methods=['GET'])
def login_process(uname, password):
    chkuser = User.objects(name=uname, insecure_password=password)
    if chkuser:
        u = chkuser[0]
        u.token = str(uuid4())
        u.save()
        resp = Response(dumps(dict(status="good", token=u.token, name=uname)))
        resp.set_cookie('token', value=u.token)
        return resp
    else:
        return dumps(dict(auth=False, issue='Bad Username/Password'))


@login.route('/logout')
def logout_proc():
    lg = Response(dumps(dict(status="loggedout")))
    lg.set_cookie('token', value='', expires=0)
    return lg



@login.route('/check')
@login.route('/who')
def somethinghere():
    genis = lambda f=False, iss="Bad Session": dumps(dict(session=f, issue=iss, name="Guest"))
    if not request.cookies or not 'token' in request.cookies:
        return genis(iss="No Session")

    q = User.objects(token=request.cookies['token'])
    if is_logged_in():
        return dumps(dict(name=q[0].name, session=True))

    return genis(iss="Bad Session")
