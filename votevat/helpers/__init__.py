from flask import request, Response
from ..models import User, BallotBox

class statusResponse(Response):
    def __init__(self, *args, **kwargs):
        super(statusResponse, self).__init__(*args, **kwargs)



def is_logged_in():
    if not request.cookies or not 'token' in request.cookies:
        return False
    q = User.objects(token=request.cookies['token'])
    if q:
        return True
    return False


def get_user():
    if not request.cookies or not 'token' in request.cookies:
        return None
    q = User.objects(token=request.cookies['token'])
    if q:
        return q[0]
    return


def user_can_change_poll(user, pollid):
    poll = BallotBox.objects(url_guid=pollid)
    if not poll:
        return False
    if user == poll[0].created_by:
        return True

def get_poll(pollid):
    poll = BallotBox.objects(url_guid=pollid)
    if poll:
        return poll[0]
    return