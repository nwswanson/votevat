from flask import Flask, url_for
from flask.ext.mongoengine import MongoEngine
import os

app = Flask(__name__)

app.config["MONGODB_SETTINGS"] = {'DB': "votevat"}

db = MongoEngine(app)

#Attach a mongo cursor to the application instance.


# ========================================================================
# eventually this should be served via nginx. and should have a config wrapper
# preventing it during production
# 
# This modifies paths so that /static/app/ looks like / to the frontend.
# 

from werkzeug import SharedDataMiddleware
app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {
  '/': os.path.join(os.path.dirname(__file__), 'static', 'app')
})
# ========================================================================


# Function to easily find your assets
# In your template use <link rel=stylesheet href="{{ static('filename') }}">
app.jinja_env.globals['static'] = (
    lambda filename: url_for('static', filename=filename)
)

app.secret_key = "terrible secret"



from votevat import views

from blueprints import login
app.register_blueprint(login)

from blueprints import votesystem
app.register_blueprint(votesystem)