#!/usr/bin/env python
from votevat import app
from flask.ext.script import Manager, Server
app.debug = True

#import logging
#log = logging.getLogger('werkzeug')
#log.setLevel(logging.ERROR)

manager = Manager(app)
manager.add_command("runlive", Server(host="0.0.0.0"))




if __name__ == "__main__":
    manager.run()